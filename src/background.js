"use strict";

// Polyfil
let menus;

if (browser.menus && typeof browser.menus.create === "function") {
    menus = browser.menus
} else {
    menus = browser.contextMenus;
}

// Basic description for menu
const menuDesc = {
    id: "sarcasm-at-text",
    title: browser.i18n.getMessage("contextMenuText"),
    contexts: ["editable", "selection"],
    enabled: true
};

// Valid tab management (only show context menu if tab runs content scripts)
let validTabs = {
    tabs: {},

    addTab(tabId) {
	this.tabs[tabId] = true;
    },

    removeTab(tabId) {
	delete this.tabs[tabId];
    },

    isValid(tabId) {
	if (this.tabs[tabId])
	    return true;

	return false;
    }
};

let contextMenu = {
    created: false,
    currentTab: null,
    enabled: true,

    setup() {
	menus.create(menuDesc, () => {
	    this.created = true;

	    this.dispatchUpdate();
	});
    },

    dispatchUpdate() {
	if (!this.created)
	    return;

	if (this.currentTab !== null && validTabs.isValid(this.currentTab))
	    this.setEnabled(true);
    },

    setEnabled(enabled) {
	if (enabled === this.enabled)
	    return;

	menus.update(menuDesc.id, {
	    enabled: enabled
	});

	this.enabled = enabled;
    },

    setCurrentTab(id) {
	this.currentTab = id;

	this.dispatchUpdate();
    }
};

contextMenu.setup();

// When a new tab is activated, enable/disable the context menu
browser.tabs.onActivated.addListener(function (activeInfo) {
    contextMenu.setCurrentTab(activeInfo.tabId);
});

// When a tab is removed, it's no longer valid
browser.tabs.onRemoved.addListener(function (tabId) {
    validTabs.removeTab(tabId);
});

// A tab is valid if it can inform the background worker that it is valid
// That means the content script was loaded
browser.runtime.onMessage.addListener(function (message, sender) {
    if (message && message.activateTab) {
	validTabs.addTab(sender.tab.id);
    }
});

// Dispatch action to content script
menus.onClicked.addListener(function (info, tab) {
    browser.tabs.sendMessage(
	tab.id,
	{ objId: info.targetElementId },
	{ frameId: info.frameId }
    );
});
