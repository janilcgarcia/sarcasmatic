"use strict";

function upper(s) {
    return s.toUpperCase();
}

function lower(s) {
    return s.toLowerCase();
}

const convertions = [upper, lower];
Object.freeze(convertions);

/**
 * An iterator-like structure that alternates between upper and lower
 * case convertion.
 */
class Convertions {
    /**
     * @param {number} initial - An integer representing the first convertion
     * to apply. This makes it easier to allow randomization of the alternating
     * order of the sarcastic tone. 0 means upper case convertion, 1 means
     * lower case convertion. Default to `0`
     */
    constructor(initial) {
	this.index = Math.abs(initial || 0) % 2;
	this.apply = this.convertions(this.index);
    }

    /**
     * Goes to next convertion type.
     */
    next() {
	this.index = (this.index + 1) % 2;
	this.apply = this.convertions(this.index);
    }

    /**
     * Get convertion based on index.
     * @param {number} i - Index for convertion, 0 = upper, 1 = lower
     */
    convertions(i) {
	return convertions[i];
    }
}

/**
 * Check if letter is affected by sarcasm, that is, can be have the alternated.
 * @param {string} c - character to test. Undefined behaviour if c.length !== 1
 * @return {boolean} - if character is a letter that has case
 */
function isSarcasticLetter(c) {
    return c.toUpperCase() !== c.toLowerCase();
}

/**
 * Transform the text into "SaRcAsTiC cAsE".
 * @param {string} text - Text to transform
 * @param {number} start - where the sarcasm starts
 * @param {number} end - where the sarcasm ends
 * @return {string} - the text in "sarcastic case". The first char may be upper
 * or lower case, it's random.
 */
function sarcasmify(text, start, end) {
    let c = new Convertions(Math.floor(Math.random() * 2));

    if (!start) {
	start = 0;
	end = text.length;
    }

    if (!end) {
	end = text.length;
    }
    
    let sarcastic = [];
    let idx = 0;
    let ch;

    while ((ch = text.charAt(idx)) !== "") {
	// If letter is not sarcastic, keep going.
	if (idx < start || idx > end || !isSarcasticLetter(ch)) {
	    sarcastic.push(ch);
	} else {
	    // If it is, alternate case
	    sarcastic.push(c.apply(ch));
	    c.next();
	}

	idx += 1;
    }

    return sarcastic.join("");
}

/**
 * Transforms contents of element into sarcasm. If any text is selected, just
 * transform that range.
 * @param {HTMLInputElement|HTMLTextArea} el - the element to sarcastisised.
 * @return {void}
 */
function sarcasmifyElement(el) {
    if (el.readOnly || el.disabled)
	return;

    let start = el.selectionStart;
    let end = el.selectionEnd;

    const length = end - start;

    if (length == 0) {
	start = undefined;
	end = undefined;
    }
    
    el.value = sarcasmify(el.value, start, end);
}

/**
 * Drill into and element (that means, get the first child which does not have
 * other children).
 * @param {Node} el - Parent node
 * @return {Node} - Returns el if el does not have children, else find the
 * first node inside el that does not have children
 */
function drillIn(el) {
    let next = el;

    while (next.childNodes.length > 0)
	next = next.childNodes[0];

    return next;
}

/**
 * Finds the next node that is a TEXT_NODE after el (or inside it).
 * @param {Node} el - Node to start from
 * @return {Node} - First TEXT_NODE found
 */
function nextTextNode(el) {
    let next;

    while (el) {
	next = drillIn(el);

	if (next !== el && next.nodeType === Node.TEXT_NODE)
	    return next;

	if (next.nextSibling) {
	    next = next.nextSibling;

	    if (next.nodeType === Node.TEXT_NODE)
		return next;

	    el = drillIn(next);
	} else {
	    el = next.parentNode;
	}
    }

    throw new Exception("Can't find next text node");
}

/**
 * Transform the selected text range into sarcasm.
 * @param {Range} range - A selection range
 */
function sarcasmifySelectionRange(range) {
    const start = range.startContainer;
    const end = range.endContainer;
    const startOff = range.startOffset;
    const endOff = range.endOffset;

    let el = start;

    function sarcasmifySection(el) {
	let cutStart = 0;
	let cutEnd = el.data.length;

	if (el === start) {
	    cutStart = startOff;
	}

	if (el === end) {
	    cutEnd = endOff;
	}

	el.data = sarcasmify(el.data, cutStart, cutEnd);
    }

    while (el !== end) {
	sarcasmifySection(el);

	el = nextTextNode(el);
    }

    sarcasmifySection(el);
}

/**
 * Get the current selected text on the tab and sarcastify it.
 */
function sarcasmifySelection() {
    const selection = window.getSelection();
    console.log(selection);

    for (let i = 0; i < selection.rangeCount; i++) {
	sarcasmifySelectionRange(selection.getRangeAt(i));
    }
}

browser.runtime.sendMessage({
    activateTab: true
});

browser.runtime.onMessage.addListener(req => {
    let el = browser.menus.getTargetElement(req.objId);

    if (el instanceof HTMLInputElement || el instanceof HTMLTextAreaElement) {
	sarcasmifyElement(el);
    } else {
	sarcasmifySelection();
    }
});
